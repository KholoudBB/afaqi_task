<?php

namespace App\Console\Commands;

use App\Vehicle;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class VehicleExpense extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vehicle:expenses';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert vehicles expenses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $vehicles = DB::table('vehicles')->orderBy('id', 'asc')->pluck('id')->take(70);

        foreach ($vehicles as $vehicle_id) {
            $vehicle = Vehicle::find($vehicle_id);
            $vehicle->fuel()->get()->map(function ($item) use ($vehicle, $vehicle_id) {
                DB::table('expenses')->insert(
                    [
                        'vehicle_id' => $vehicle_id,
                        'name' => $vehicle->name,
                        'plate_number' => $vehicle->plate_number,
                        'cost' => $item->cost,
                        'type' => 'fuel',
                        'created_date' => $item->entry_date
                    ]
                );
            });
            $vehicle->insurancePayment()->get()->map(function ($item) use ($vehicle, $vehicle_id) {
                DB::table('expenses')->insert(
                    [
                        'vehicle_id' => $vehicle_id,
                        'name' => $vehicle->name,
                        'plate_number' => $vehicle->plate_number,
                        'cost' => $item->amount,
                        'type' => 'insurance',
                        'created_date' => $item->contract_date
                    ]
                );
            });
            $vehicle->insurancePayment()->get()->map(function ($item) use ($vehicle, $vehicle_id) {
                DB::table('expenses')->insert(
                    [
                        'vehicle_id' => $vehicle_id,
                        'name' => $vehicle->name,
                        'plate_number' => $vehicle->plate_number,
                        'cost' => $item->amount,
                        'type' => 'service',
                        'created_date' => $item->created_at
                    ]
                );
            });


        }
    }
}
