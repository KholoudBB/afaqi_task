<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

class ExpensesResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request
     * @return array
     */

    public function toArray($request)
    {

        return [
            'id'            =>$this->vehicle_id,
            'name'          =>$this->name,
            'plate_number'  =>$this->plate_number,
            'cost'          =>$this->cost,
            'type'          => $this->type,
            'created_date'  =>$this->created_date,
        ];

    }


}
