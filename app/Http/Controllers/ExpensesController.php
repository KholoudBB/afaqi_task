<?php

namespace App\Http\Controllers;


use App\Http\Resources\ExpensesCollection;
use App\Jobs\ExpensesJob;
use Illuminate\Support\Facades\DB;

class ExpensesController extends Controller
{

    public function getExpensesList()
    {
        $expense_query = DB::table('expenses');

         /**
          apply filters
         **/

        if (request('type')) {
            $type = request('type');
            $expense_query->when($type, function ($query, $type) {
                return $query->where('type', $type);
            });
        }

        if (request('name')) {
            $name = request('name');
            $expense_query->when($name, function ($query, $name) {
                return $query->where('name', 'like', '%' . $name . '%');
            });
        }

        if (request('cost'))
             $this->costSort($expense_query);

        if (request('min_cost') || request('max_cost'))
             $this->costFilter($expense_query);

        if (request('date'))
             $this->dateSort($expense_query);

        if (request('min_date') || request('max_date'))
            $this->dateFilter($expense_query);

        return new ExpensesCollection($expense_query->orderBy('vehicle_id', 'asc')->get());

    }


    public function costSort($expense_query)
    {

        if (request('cost') == 'min') {
            $expense_query->where('cost', DB::raw("(select min(`cost`) from expenses)"));

        } elseif (request('cost') == 'max') {
            $expense_query->where('cost', DB::raw("(select max(`cost`) from expenses)"));

        }

        return $expense_query;

    }

    public function costFilter($expense_query)
    {
        if (request('min_cost') && request('max_cost')) {

            $expense_query->where([
                ['cost', '>', request('min_cost')],
                ['cost', '<', request('max_cost')],
            ]);

        }

        if (request('min_cost')) {
            $expense_query->where('cost', '>', request('min_cost'));
        }

        if (request('max_cost')) {
            $expense_query->where('cost', '<', request('max_cost'));

        }

        return $expense_query;

    }

    public function dateSort($expense_query)
    {

        if (request('date') == 'asc') {
            $expense_query->orderBy('created_date', 'asc');

        } elseif (request('date') == 'desc') {
            $expense_query->orderBy('created_date', 'desc');

        }
        return $expense_query;

    }

    public function dateFilter($expense_query)
    {
        if (request('min_date') && request('max_date')) {

            $expense_query->whereBetween('created_date',[request('min_date'),request('max_date')]);
        }
        if (request('min_date')) {
            $expense_query->whereDate('created_date', '>=', request('min_date'));

        }
        if (request('max_date')) {
            $expense_query->whereDate('created_date', '<=', request('max_date'));

        }

        return $expense_query;

    }

}
