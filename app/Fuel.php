<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Fuel extends Model
{

    protected $table = 'fuel_entries';


    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];


    /**
     * Get the vehicle record associated with this record.
     */
    public function vehicle()
    {
        return $this->belongsTo(vehicle::class, 'vehicle_id', 'id');
    }


   }
