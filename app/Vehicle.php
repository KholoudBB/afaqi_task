<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];



    /**
     * get ExpensesCollection's fuelRecords
     */

    public function fuel()
    {

        return $this->hasMany(Fuel::class);

    }

    /**
     * get ExpensesCollection's insurance_payment
     */

    public function insurancePayment()
    {

        return $this->hasMany(InsurancePayment::class);

    }

    /**
     * get ExpensesCollection's service Records
     */

    public function services()
    {

        return $this->hasMany(Service::class);

    }



}
