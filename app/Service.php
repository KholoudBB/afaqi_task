<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{


    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];



    public function vehicle()
    {

        return $this->belongsTo(Vehicle::class);
    }


}
